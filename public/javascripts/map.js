//Utilizamos la api de mapbox. Para utilizar su mapa.
var mymap = L.map('main_map').setView([-34.90635202391138, -56.18481263112612], 13); //Generamos la vinculación con la api, en donde configuraremos su vista a nuestras coordenadas geográficas elegidas y un nivel de zoom determinado. setView devuelve el objeto del mapa


L.tileLayer('http://api.mapbox.com/styles/v1/mapbox/streets-v11/tiles/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZmFjdXZhY2EiLCJhIjoiY2t0ajV4cDkwMTh5MzJ2bXBncng2Y2JmbyJ9.SUugxMJ3cbO0p0mFGe0Ovg', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',    
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoiZmFjdXZhY2EiLCJhIjoiY2t0ajV4cDkwMTh5MzJ2bXBncng2Y2JmbyJ9.SUugxMJ3cbO0p0mFGe0Ovg'
}).addTo(mymap);


L.control.scale().addTo(mymap); //Agregamos un indicador para saber la altura de la vista 

// var marker = L.marker([-34.91338945928982, -56.168162496387815]).addTo(mymap);
// var marker2 = L.marker([-34.89397812803235, -56.15334635907768]).addTo(mymap);
// var marker3 = L.marker([-34.90655289452681, -56.199827672501264]).addTo(mymap);
//Con estos tres generamos 3 marcadores en el mapa con sus respectivas coordenadas. Con el método addTo() y el parametro de nuestro mapa, es para añadir las localizaciones a nuestro mapa 




fetch('http://localhost:3000/api/bicicletas')
.then(res => res.json())
.then(data => {
    data.bicicletas.forEach((bicicleta) => {
        L.marker(bicicleta.ubicacion, {title: bicicleta.id}).addTo(mymap)
        .bindPopup(`Bicicleta color: ${bicicleta.color}`)
        .openPopup();
    })
})