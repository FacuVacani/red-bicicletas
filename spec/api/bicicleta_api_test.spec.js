var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');
//al traernos el server, se generá que se abre el servidor y se cierra automaticamente al realizar un testeo.


describe('Bicicleta API', () =>{
    describe('GET BICICLETAS /', ()=>{
        it('Status 200', () => {
            expect(Bicicleta.allBicis.length).toBe(0);
            var a = new Bicicleta(1, "rojo", "urbana", [-34.91338945998982, -56.168182496387815]);
            Bicicleta.add(a);

            request.get('http://localhost:3000/api/bicicletas', function(error, response, body){
                expect(response.statusCode).toBe(200)
            });
        });
    });

    describe('POST BICICLETAS /create', ()=>{
        it('STATUS 200', (done) => {
            var headers = {'content-type:' : "application/json"};
            var aBici = '{ "id": 10, "color": "rojo", "modelo", "urbana", "lat": -34, "lng": -56}';
            request.post({
                headers: headers,
                url: 'http://localhost:3000/api/bicicletas/create',
                body: aBici
            }, function(error, response, body){
                    expect(response.statusCode).toBe(200);
                    expect(Bicicleta.finById(10).color).toBe("rojo");
                    done(); //Hasta que done no se ejecuta, el test no se termina. Ya que este request es Asincronico (todo en nodejs es asincronico)
            });
        });
    });
});