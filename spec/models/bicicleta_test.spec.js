var Bicicleta = require('../../models/bicicleta');

beforeEach(() => {Bicicleta.allBicis = []; }) 
describe('Bicicleta.allBicis', ()=> {
    it("comienza vacia", () => {
        expect(Bicicleta.allBicis.length).toBe(0)
    });
}); // Describe va a ser foco en algun comportamiento puntual, recibe dos parametros, uno será una descripción del método que se quire testear, y por segundo parametro una función. Se utiliza el método it() que será lo que se quiere probar. Se le ingresan también dos parametros, uno será el como comenzará (en este caso comienza vacia) en string, y por segundo parametro una función, la cual tendrá un método llamado expect(), seguido de este como propiedad metodo, toBe(). Expect será lo que estamos esperando. Y el toBe lo que será que esperemos

describe('Bicicleta.add', () =>{
    it("agregamos una", () => {
        expect(Bicicleta.allBicis.length).toBe(0)

        var a = new Bicicleta(1, "rojo", "urbana", [-34.91338945998982, -56.168182496387815]);
        Bicicleta.add(a);
        expect(Bicicleta.allBicis.length).toBe(1)
        expect(Bicicleta.allBicis[0]).toBe(a)

    });
});


describe('Bicicleta.findById', () =>{
    it("debe devolver la bici con id 1" , () => {
        // Bicicleta.allBicis = [];    //aquí reseteamos "blanqueamos" la colección de bicis antes de realizar este test, ya que sino se enlazaría con el anterior test y daría error. comenzando con el elemento del bloque de arriba. Se debería hacer en cada bloque de test que se haga. Pero jasmine ya tiene de por si un método que hace esto. Este es beforeEach. En este caso será: beforeEach(() => {Bicicleta.allBicis = []; })  Y se lo pondrá al iniciar el testeo general.

        expect(Bicicleta.allBicis.length).toBe(0)

        var aBici = new Bicicleta(1, "verde", "urbana", [-34.91338945998982, -56.168182496387651]);
        var aBici2 = new Bicicleta(2, "violeta", "motaña", [-34.91338945998982, -56.168182496387903]);
        Bicicleta.add(aBici);
        Bicicleta.add(aBici2);
        var targetBici = Bicicleta.finById(1);
        expect(targetBici.id).toBe(1);
        expect(targetBici.color).toBe(aBici.color);
        expect(targetBici.modelo).toBe(aBici.modelo);


    });
});

