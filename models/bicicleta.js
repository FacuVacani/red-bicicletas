var Bicicleta = function (id, color, modelo, ubicacion){
    this.id = id;
    this.color = color;
    this.modelo = modelo;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function (){
    return "id: " + this.id + " | color: " + this.color;
}

Bicicleta.allBicis = [];
Bicicleta.add = function(aBici){
    Bicicleta.allBicis.push(aBici); //push() agrega un elemento al final de un array u objeto
}

Bicicleta.finById = function(aBiciId){
    var aBici = Bicicleta.allBicis.find(x => x.id == aBiciId);
    if (aBici)
        return aBici;
    else
        throw new Error(`No existe una bicicleta con el id ${aBiciId}`);
}


Bicicleta.removeById = function(aBiciId){

    for(var i = 0; i < Bicicleta.allBicis.length; i++){
        if(Bicicleta.allBicis[i].id == aBiciId){
            Bicicleta.allBicis.splice(i, 1); //splice() elimina un elemento en dicha posición de un array u objeto
            break;
        }
    }
}

// var a = new Bicicleta(1, "rojo", "urbana", [-34.91338945998982, -56.168182496387815]);
// var b = new Bicicleta(2, "blanca", "urbana", [-34.90655289452702, -56.199827672501311]);

// Bicicleta.add(a);
// Bicicleta.add(b);

module.exports = Bicicleta;